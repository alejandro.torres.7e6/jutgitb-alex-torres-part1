import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import java.util.Scanner
import java.io.File
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import org.postgresql.util.PSQLException
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

/*
* AUTHOR: Alejandro Torres Lemos
* DATE: 15/02/2023
*/

val scanner = Scanner(System.`in`)
val dbUrl = "jdbc:postgresql://localhost:5432/jutge"
//val llistaProblemesJson = jsonALlistaProblemes()
val llistaProblemes = mutableListOf<Problema>()

fun jsonALlistaProblemes(): MutableList<Problema> {
    val mapper = jacksonObjectMapper()
    mapper.registerKotlinModule()

    val jsonString: String = File("problemes.json").readText(Charsets.UTF_8)
    return mapper.readValue(jsonString)
}
fun escriureFitxerJson(llistaProblemes: List<Problema>) {

    val json = File("problemes.json")

    json.writeText("[\n")
    for (i in 0 until  llistaProblemes.lastIndex){
        val productToJson = Json.encodeToJsonElement(llistaProblemes[i]).toString()
        json.appendText("$productToJson,\n")
    }
    json.appendText("${Json.encodeToJsonElement(llistaProblemes.last())}\n]")
}

fun main(){
    val connection = DriverManager.getConnection(dbUrl)

//    afegirProblemesADb(connection)
    llegirProblemaDB(connection)
    typeOfUser(connection)
}

fun llegirProblemaDB(connection: Connection) {

    try {
        var enunciatProblema: String
        var idProblema: Int
        var problemaResolt: Boolean
        var intents: Int
        var jocDeProvesPublic = JocDeProves("", "")
        var jocDeProvesPrivat = JocDeProves("", "")


        val querySelectTotsElsProblemes = "SELECT * FROM problema"
        val querySelectElJocDeProvesPublicById = "SELECT * FROM jocdeprovespublic WHERE id_problema = ?"
        val querySelectElJocDeProvesPrivatById = "SELECT * FROM jocdeprovesprivat WHERE id_problema = ?"
        val querySelectTotsIntentsFetsDelProblemaById = "SELECT * FROM intent WHERE id_problema = ?"


        connection.prepareStatement(querySelectTotsElsProblemes).use { statementProblema ->
            val resultProblema = statementProblema.executeQuery()
            while (resultProblema.next()) {
                enunciatProblema = resultProblema.getString("enunciat")
                idProblema = resultProblema.getInt("id_problema")
                problemaResolt = resultProblema.getBoolean("resolt")
                intents = resultProblema.getInt("intents")

                connection.prepareStatement(querySelectElJocDeProvesPublicById).use { statementJocDeProvesPublic ->
                    statementJocDeProvesPublic.setInt(1, idProblema)
                    val resultJocDeProvesPublic = statementJocDeProvesPublic.executeQuery()
                    while (resultJocDeProvesPublic.next()) {
                        val jocdeProvesPublicInput = resultJocDeProvesPublic.getString("input")
                        val jocdeProvesPublicOutput = resultJocDeProvesPublic.getString("output")
                        jocDeProvesPublic = JocDeProves(jocdeProvesPublicInput, jocdeProvesPublicOutput)
                    }
                }
                connection.prepareStatement(querySelectElJocDeProvesPrivatById).use { statementJocDeProvesPrivat ->
                    statementJocDeProvesPrivat.setInt(1, idProblema)
                    val resultJocDeProvesPrivat = statementJocDeProvesPrivat.executeQuery()
                    while (resultJocDeProvesPrivat.next()) {
                        val jocDeProvesPrivatInput = resultJocDeProvesPrivat.getString("input")
                        val jocDeProvesPrivatOutput = resultJocDeProvesPrivat.getString("output")
                        jocDeProvesPrivat = JocDeProves(jocDeProvesPrivatInput, jocDeProvesPrivatOutput)
                    }
                }
                connection.prepareStatement(querySelectTotsIntentsFetsDelProblemaById).use { statementIntent ->
                    statementIntent.setInt(1, idProblema)
                    val resultIntent = statementIntent.executeQuery()
                    val llistaIntents = mutableListOf<Intent>()
                    while (resultIntent.next()) {
                        val intentRespota = resultIntent.getString("sortida")
                        llistaIntents.add(Intent(intentRespota))
                    }
                    val problema = Problema(
                        idProblema,
                        enunciatProblema,
                        jocDeProvesPublic,
                        jocDeProvesPrivat,
                        problemaResolt,
                        intents,
                        llistaIntents
                    )
                    llistaProblemes.add(problema)
                }
            }
        }
    }  catch (e: PSQLException) {
        println("Error: ${e.message}")
    }

}

//fun afegirProblemesADb(connection: Connection) {
//
//    val sqlTaulaProblema = "INSERT INTO problema (enunciat, resolt, intents) VALUES (?, ?, ?)"
//    val sqlTaulaJocDeProvesPublic = "INSERT INTO jocdeprovespublic (id_problema, input, output) VALUES (?, ?, ?)"
//    val sqlTaulaJocDeProvesPrivat = "INSERT INTO jocdeprovesprivat (id_problema, input, output) VALUES (?, ?, ?)"
////    val sqlTaulaIntent = "INSERT INTO intent (id_problema, resposta, timestamp) VALUES (?, ?, ?)"
//
//    var index = 1
//    llistaProblemesJson.forEach {
//
//        val taulaProblemaStatement = connection.prepareStatement(sqlTaulaProblema)
//        taulaProblemaStatement.setString(1, it.enunciat)
//        taulaProblemaStatement.setBoolean(2, false)
//        taulaProblemaStatement.setInt(3, 0)
//        taulaProblemaStatement.executeUpdate()
//
//        val taulaJocDeProvesPublicStatement = connection.prepareStatement(sqlTaulaJocDeProvesPublic)
//        taulaJocDeProvesPublicStatement.setInt(1, index)
//        taulaJocDeProvesPublicStatement.setString(2, it.jocDeProvesPublic.input)
//        taulaJocDeProvesPublicStatement.setString(3, it.jocDeProvesPublic.output)
//        taulaJocDeProvesPublicStatement.executeUpdate()
//
//        val taulaJocDeProvesPrivatStatement = connection.prepareStatement(sqlTaulaJocDeProvesPrivat)
//        taulaJocDeProvesPrivatStatement.setInt(1, index)
//        taulaJocDeProvesPrivatStatement.setString(2, it.jocDeProvesPrivat.input)
//        taulaJocDeProvesPrivatStatement.setString(3, it.jocDeProvesPrivat.output)
//        taulaJocDeProvesPrivatStatement.executeUpdate()
//
////        val taulaIntentStatement = connection.prepareStatement(sqlTaulaIntent)
////        taulaIntentStatement.setInt(1, index)
////        taulaIntentStatement.setString(2, it.jocDeProvesPrivat.input)
////        taulaIntentStatement.setString(3, it.jocDeProvesPrivat.output)
////        taulaIntentStatement.executeUpdate()
//
//        index++
//    }
//}

fun modificarProblemaDeDb(connection: Connection, enunciat: String, resolt: Boolean, intents: Int, id: Int) {

    try{
        val queryUpdateProblem = "UPDATE problema SET enunciat = ?, resolt = ?, intents = ? WHERE id_problema = ?"
        val preparedStatementProblema = connection.prepareStatement(queryUpdateProblem)
        preparedStatementProblema.setString(1, enunciat)
        preparedStatementProblema.setBoolean(2, resolt)
        preparedStatementProblema.setInt(3, intents)
        preparedStatementProblema.setInt(4, id)

        preparedStatementProblema.executeUpdate()

    } catch (e: PSQLException){
        println("Error $e")
    }
}

fun afegirIntentADb(escanner: String, id: Int, connection: Connection) {
    val queryInsertIntent = "INSERT INTO intent (id_problema, sortida) VALUES (?, ?)"

    try {
        val preparedStatementIntent = connection.prepareStatement(queryInsertIntent)
        preparedStatementIntent.setInt(1, id)
        preparedStatementIntent.setString(2, escanner)

        preparedStatementIntent.executeUpdate()

    } catch (e: PSQLException) {
        println("Error: ${e.message}")
    }
}

fun typeOfUser(connection: Connection){
    println(showTypeOfUser())
    val scanner = Scanner(System.`in`)
    val escanner = scanner.next()
    if (escanner == "1"){
        chooseOptionOfGame(connection)
    }
    else if (escanner == "2"){
        chooseOptionOfGameTeacher(connection)
    }
    else typeOfUser(connection)
}
fun showTypeOfUser(): String{
    return("<--------------------JUTGITB--------------------->\n" +
            "1 -> Sóc alumne\n" +
            "2 -> Sóc professor")
}
fun askVerifyProblem(firstTime : Boolean, connection: Connection): Boolean {
    val scanner = Scanner(System.`in`)
    if (firstTime){
        println("Vols resoldre el problema? [SI / NO]")
    }
    else println("Vols tornar a provar [SI / NO]")
    val escanner = scanner.next().uppercase()

    if (escanner == "SI" || escanner == "SÍ") {
        return true
    }
    else if (escanner=="NO"){
        chooseOptionOfGame(connection)
        return false
    }
    else
        return askVerifyProblem(firstTime, connection)
}

fun chooseOptionOfGame(connection: Connection) {
    println(showMenuAlumne())
    val escanner = Scanner(System.`in`)
    var chooseOption = escanner.next()
    while (chooseOption != "0") {
        when (chooseOption) {
            "1" ->learningitinerary(jsonALlistaProblemes(), connection)
            "2" -> println(listOfProblems(connection))
            "3" -> studentHistory()
            "4" -> println(showAjuda())
            "5" -> typeOfUser(connection)
            else -> println("Valor no valido\n")
        }
        println(showMenuAlumne())
        chooseOption= escanner.next()
    }
}
fun chooseOptionOfGameTeacher(connection: Connection) {
    var apted = false
    val contrasenya = "123456"

    while (!apted){
        print("Contrasenya: ")
        val scannerContrasenya = scanner.next()
        if (contrasenya == scannerContrasenya){
            apted = true
        }
        else println("Contrasenya Incorrecta\n")
    }

    println(showMenuProfesor())
    var chooseOption = scanner.next()
    while (chooseOption != "0") {
        when (chooseOption) {
            "1" ->addProblemsTeacher(connection)
            "2" ->addMark()
            "3" -> typeOfUser(connection)
            else -> println("Valor no valido\n")
        }
        println(showMenuAlumne())
        chooseOption= scanner.next()
    }
}

fun showMenuAlumne(): String{
    return(
            "1 -> Seguir amb l'itinerari d'aprenentatge \n" +
            "2 -> Llista problemes\n" +
            "3 -> Consultar històric de problemes resolts\n" +
            "4 -> Ajuda\n" +
            "5 -> Sortir")
}
fun showMenuProfesor(): String{
    return(
                    "1 -> Afegir nous problemes \n" +
                    "2 -> Treure un report de la feina de l'alumne\n" +
                            "3 -> Escollir usuari"
            )
}
fun learningitinerary(llistaProblemes: MutableList<Problema>, connection: Connection) {
    for (i in 0 ..llistaProblemes.lastIndex) {
        if (!llistaProblemes[i].resolt) {
            val problema = llistaProblemes[i]
            println(problema.showEnunciat())
            askVerifyProblem(true, connection)
            println("Ara resolt aquest cas:")
            problema.checkAnswer(connection)
        }
    }
}

fun studentHistory() {
    for (problema in llistaProblemes) {
        if (problema.resolt) {
            println(
                "\n${problema.id}-> ${problema.enunciat}\n" +
                        "INTENTS -> ${problema.llistaIntents}\n" +
                        "ESTAT-> RESOLT"
            )
        }
    }
    showMenuAlumne()

}

fun listOfProblems(connection: Connection){
    var id = 1
//    studentHistory(jsonALlistaProblemes())
    for (problema in llistaProblemes){
        println("$id -> ${problema.enunciat}")
        id++
    }
    println("0 -> Sortir")
    val scanner = Scanner(System.`in`)
    val escanner = scanner.nextInt()
    if (escanner == 0){
        chooseOptionOfGame(connection)
    }
    askVerifyProblem(true, connection)
    llistaProblemes[escanner-1].checkAnswer(connection)
    listOfProblems(connection)
}
fun showAjuda(): String {
    return("\nAquest programa et permet fer diferents accions per resoldre problemes.\n" +
            "Per exemple et permet seguir amb els problemes si ho havies deixat a mitjes, també et permet veure la llista de problemes que hi ha i escollir un concret.\n" +
            "Per ultim pots veure un historic de cada problema que hi ha amb els intents corresponents i si han sigut correcte o no.\n"
            )
}
fun addProblemsTeacher(connection: Connection) {
    val scanner = Scanner(System.`in`)

    val lastIndex = llistaProblemes.lastIndex + 1


    val sqlTaulaProblema = "INSERT INTO problema (enunciat, resolt, intents) VALUES (?, ?, ?)"
    val sqlTaulaJocDeProvesPublic = "INSERT INTO jocdeprovespublic (id_problema, input, output) VALUES (?, ?, ?)"
    val sqlTaulaJocDeProvesPrivat = "INSERT INTO jocdeprovesprivat (id_problema, input, output) VALUES (?, ?, ?)"



    println("Introdueix l'enunciat del nou problema que vols afegir:")
    val enunciat = scanner.nextLine()
    println("Introdueix el input del joc de proves public:")
    val inputPublic= scanner.nextLine()
    println("Introdueix el output del joc de proves public:")
    val outputPublic = scanner.nextLine()
    println("Introdueix el input del joc de proves privat:")
    val inputPrivat = scanner.nextLine()
    println("Introdueix el output del joc de proves privat:")
    val outputPrivat = scanner.nextLine()


        val taulaProblemaStatement = connection.prepareStatement(sqlTaulaProblema)
        taulaProblemaStatement.setString(1, enunciat)
        taulaProblemaStatement.setBoolean(2, false)
        taulaProblemaStatement.setInt(3, 0)
        taulaProblemaStatement.executeUpdate()

        val taulaJocDeProvesPublicStatement = connection.prepareStatement(sqlTaulaJocDeProvesPublic)
        taulaJocDeProvesPublicStatement.setInt(1, lastIndex)
        taulaJocDeProvesPublicStatement.setString(2, inputPublic)
        taulaJocDeProvesPublicStatement.setString(3, outputPublic)
        taulaJocDeProvesPublicStatement.executeUpdate()

        val taulaJocDeProvesPrivatStatement = connection.prepareStatement(sqlTaulaJocDeProvesPrivat)
        taulaJocDeProvesPrivatStatement.setInt(1, lastIndex)
        taulaJocDeProvesPrivatStatement.setString(2, inputPrivat)
        taulaJocDeProvesPrivatStatement.setString(3, outputPrivat)
        taulaJocDeProvesPrivatStatement.executeUpdate()


    llistaProblemes.add(Problema(lastIndex, enunciat, JocDeProves(inputPublic, outputPublic), JocDeProves(inputPrivat, outputPrivat), false, 0, mutableListOf(), ))
    println("Problema afegit!")

    escriureFitxerJson(llistaProblemes)
    chooseOptionOfGame(connection)
}


fun addMark() {
    for (problema in llistaProblemes) {
        if (problema.resolt) {
            println(
                "\n${problema.id} -> ${problema.enunciat}\n" +
                        "INTENTS -> ${problema.intents}"
            )

            if (problema.intents in 1..11) {
                println("NOTA -> ${11 - problema.intents}")
            } else {
                println("NOTA -> 0")
            }
        }
    }
}

