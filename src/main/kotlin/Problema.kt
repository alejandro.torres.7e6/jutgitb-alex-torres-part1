import kotlinx.serialization.Serializable
import java.sql.Connection

@Serializable
class Problema(
    var id: Int,
    val enunciat: String,
    val jocDeProvesPublic: JocDeProves,
    val jocDeProvesPrivat: JocDeProves,
    var resolt: Boolean,
    var intents: Int,
    var llistaIntents: MutableList<Intent>,
) {
    fun showEnunciat(): String {
        return ("$enunciat\n" +
                "EXEMPLE: \n" +
                "${jocDeProvesPublic.input} ---> " +
                "${jocDeProvesPublic.output} "
                )
    }

    fun checkAnswer(connection: Connection) {
        print("${jocDeProvesPrivat.input} ---> ")
        val escanner = scanner.next().uppercase()


        afegirIntentADb(escanner, id, connection)

        val queryAddIntent = "INSERT INTO intent (id_problema, sortida) VALUES (?, ?)"
        val preparedStatementIntent = connection.prepareStatement(queryAddIntent)
        preparedStatementIntent.setInt(1, id)
        preparedStatementIntent.setString(2, escanner)

        preparedStatementIntent.executeUpdate()

        if (escanner == jocDeProvesPrivat.output) {
            resolt = true
            println("\u001B[32mRESPOSTA CORRECTA\u001B[0m \n")
            intents++

        } else {
            println("\u001B[31mRESPOSTA INCORRECTE\u001B[0m")
            intents++
            if (askVerifyProblem(false, connection)) {
                println("Intents: $intents")
                println("\nIntrodueix la resposta:")
            }
//            checkAnswer(connection)
        }
        modificarProblemaDeDb(connection, enunciat, resolt, intents, id)
    }
}