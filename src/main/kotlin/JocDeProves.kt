import kotlinx.serialization.Serializable

@Serializable
class JocDeProves (var input : String, var output : String)