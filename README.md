Contenido
========

 * [Titulo del proyecto](#Titulo)
 * [Instalación del proyecto](#Instalación)
 * [Ejecutar el programa](#Ejecucción)
 * [Autors](#Autors)

### Titulo:
---

JutgITB


### Instalación:
---

Para instalar el programa tienes que entrar en este link:

```bash
git clone https://gitlab.com/alejandro.torres.7e6/jutgitb-alex-torres-part1.git
```
### Ejecucción:
---

Para ejecutar el programa simplemente tienes que ejecutar el fichero 'main.kt'.


### Autors
---
Alejandro Torres Lemos
